import mss


with mss.mss() as sct:
    output = "shots/Screen-{mon}-{date:%Y%m%d_%H%M%S}.png"
    sct.shot(output=output)

    for filename in sct.save(0,output):
        print(filename)
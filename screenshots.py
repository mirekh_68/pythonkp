import mss
from pynput import keyboard, mouse
import pyautogui

import logging
import pprint

logging.basicConfig(filename=("keylog.txt"), level=logging.DEBUG, format=" %(asctime)s - %(message)s")


def on_keypressed(key):
    msg = "pressed: " + str(key)
    logging.info(msg)
    print(msg)


def on_keyreleased(key):
    msg = "released: " + str(key)
    logging.info(msg)
    print(msg)


#
# mss
#
# https://nitratine.net/blog/post/how-to-take-a-screenshot-in-python-using-mss/
#
# main screen shot: 

def scrShotAll():
    with mss.mss() as sct:
        sct.shot(output="main-screen-mss.jpg")

        # multiple screens work:
        monitors = sct.monitors  # monitors[1]  # Identify the display to capture
        print(type(monitors))
        pprint.pprint(monitors)
        for monitor in monitors:
            scrshot = sct.grab(monitor)
            print(type(scrshot))
            pprint.pprint(scrshot)
            i = monitors.index(monitor)
            sct.shot(mon=i, output=f"screen-{i}.png")
        for filename in sct.save():
            print(filename)

exit(0)

with keyboard.Listener(on_press=on_keypressed, on_release=on_keyreleased) as kbd_listener:
    kbd_listener.join()

# class Dummy:
#     #__init__
#     def fake(self):
#         print("fake!")


# with Dummy() as D:
#     D.fake()

#! /usr/bin/python3

import datetime as dt
import time as tm
import os
import mss


def makeShots(filename):
    with mss.mss() as sct:
        sct.shot(mon=0,output=filename)


def start_file_Loop():
    Start = tm.localtime()
    start = tm.time()
    print(Start)
    str_time = tm.strftime("%Y%m%d_%H%M%S",Start)
    print("Current dir:", os.getcwd())
    os.mkdir(str_time)
    os.chdir(str_time)
    print("Current dir:", os.getcwd())
    while True:
        now = tm.time() # dt.datetime.now()
        tdelta =  int(now - start)
        print(tdelta)
        filename = "screen-{mon}-{date:%Y%m%d_%H%M%S}.png"
        print(filename)
        #makeShots(filename)
        with mss.mss() as sct:
            sct.compression_level = 9
            sct.shot(mon=0,output=filename)
        #with open(filename,'w') as f:
        #    f.write(f"Hello, now we have time: {now}")

        tm.sleep(2.0)

print(__name__)


from Xlib.display import Display

def printWindowHierrarchy(window, indent):
    children = window.query_tree().children
    for w in children:
        print(indent, w.get_wm_class(), w.get_wm_name())
        printWindowHierrarchy(w, indent+'-')



def main():
    display = Display()
    root = display.screen().root
    printWindowHierrarchy(root, '-')


if __name__ == "__main__":
    start_file_Loop()


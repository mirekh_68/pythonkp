from pynput import keyboard, mouse

# https://pynput.readthedocs.io/en/latest/mouse.html#monitoring-the-mouse
#see also Dave Bombal  https://www.youtube.com/watch?v=XKoTwepEzPI


def on_mouse_click():
    pass

def on_press(key):
    try:
        print('alphanumeric key {0} pressed'.format(
            key.char))
    except AttributeError:
        print('special key {0} pressed'.format(
            key))


def on_release(key):
    print('{0} released'.format(
        key))
    if key == keyboard.Key.esc:
        # Stop listener
        return False


# Collect events until released
with keyboard.Listener(
        on_press=on_press,
        on_release=on_release) as listener:
    listener.join()

# ...or, in a non-blocking fashion:
listener = keyboard.Listener(
    on_press=on_press,
    on_release=on_release)
listener.start()

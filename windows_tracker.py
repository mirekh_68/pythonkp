import win32gui
from pynput import keyboard, mouse
import time
from my_inspect import report
import threading
import enum


def on_press(key):
    try:
        print('alphanumeric key {0} pressed'.format(
            key.char))
    except AttributeError:
        print('special key {0} pressed'.format(
            key))


class KbdListener(keyboard.Listener):
    def __init__(self):
        super().__init__(on_press=self.on_press)
        self.words = []
        self.curr_word = ''
        self.timer = threading.Timer(0.1, self.on_timer)
        self.time = 0

    class Update(enum.Enum):
        Timer = 0
        KeyPress = 1
        KeyRelease = 2
        MousePress = 3
        MouseRelease = 4

    def update_state(self, update_obj):
        self.time += 1

    def on_timer(self):
        self.update_state()

    def on_press(self, key):
        self.update_state()
        report(key)

        if hasattr(key, 'char'):
            self.curr_word += key.char
        else:
            self.words.append(self.curr_word)
            self.curr_word = ''

    def __exit__(self, exc_type, exc_val, exc_tb):
        print(self.words)
        super().__exit__(exc_type, exc_val, exc_tb)


win = None
focus_win = None

with  KbdListener() as listener:
    while True:
        new_win = win32gui.GetForegroundWindow()
        new_focus_win = win32gui.GetFocus()
        if new_win != win:
            win = new_win
            report(new_win)
            window_text = win32gui.GetWindowText(win)
            print(window_text)
        if new_focus_win != focus_win:
            focus_win = new_focus_win
            report(focus_win)
            window_text = win32gui.GetWindowText(focus_win)
            print(window_text)
        time.sleep(0.12)

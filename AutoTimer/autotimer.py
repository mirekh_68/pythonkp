#from __future__ import print_function
import time
from os import system
from activity import *
import json
import datetime
import sys


from platform_select import pfm

def url_to_name(url):
    string_list = url.split('/')
    return url #string_list[2]


class ActivityTracker:
    def __init__(self):
        self.curr_win = None
        self.prev_win = None
        self.activityList = AcitivyList([])
        try:
            self.activityList.initialize_me()
        except Exception:
            print('No json')

    def main_loop(self):
        try:
            while True:
                self.curr_win = pfm.get_active_window()
                if self.curr_win != self.prev_win:
                    self.inspect_window(self.curr_win)

                    self.activityList.process_activity(self.curr_win.text)

                    self.prev_win = self.curr_win

                time.sleep(3.5)

        except KeyboardInterrupt:
            with open('activities.json', 'w') as json_file:
                json.dump(self.activityList.serialize(), json_file, indent=4, sort_keys=True)

    def inspect_window(self,win):
        print(win)
        winInspector = pfm.WinInspector(win)
        winInspector.inspect(win)

# at = ActivityTracker()
# at.main_loop()
# exit()

import uiautomation as auto

class ControlChildrenEnum:
    pass

def inspect_ctrl(control, prefix = ''):
    print(prefix, control)
    children = control.GetChildren()
    for child in children:
        inspect_ctrl(child, prefix + '  ')

prev_ctrl = None    

while True:
    control = auto.GetFocusedControl()
    if control != prev_ctrl:
        inspect_ctrl(control)
        prev_ctrl = control
    
    # print("control: ",control)
    # controlList = []
    # ind = ' '
    # while control:
    #     controlList.insert(0, control)
    #     #control = control.GetParentControl()
    #     children = control.GetChildren()

    #     ind = ind + ' '
    #     print(ind, "control:" ,control)
        
    # control = controlList[0 if len(controlList) == 1 else 1]
        
    # address_control = auto.FindControl(control, lambda c, d: 
    #                                             isinstance(c, auto.EditControl))

    # print('Current URL:')
    # print(address_control.GetValuePattern().Value)
    time.sleep(2)

class WindowBase:
    def __init__(self, hwnd):
        self.hwnd = hwnd
        self.text = None #win32gui.GetWindowText(hwnd)
        self.class_name =None#= win32gui.GetClassName(hwnd)
        self.props = {}
        self.children = []

    def __eq__(self, other):
        if type(self) is not type(other):
            return False
        return self.hwnd == other.hwnd and self.text == other.text

    def __neq__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        return f"[{hex(self.hwnd)} : {self.text}]"



import win32gui
import uiautomation as auto

from Window import WindowBase

# also see here: https://stackoverflow.com/questions/59595763/get-active-chrome-url-in-python

def EnumPropsFunc(hwnd, propname, propdatahandle, win_obj):
    win_obj.props[propname] = propdatahandle
    return True

def UrlExtract(hwnd):
    control = auto.ControlFromHandle(hwnd)
    edit = control.EditControl()
    vpat = edit.GetValuePattern()
    val = vpat.Value
    return val



class Window(WindowBase):
    def __init__(self, hwnd):
        super().__init__(hwnd)
        self.text = win32gui.GetWindowText(hwnd)
        self.class_name = win32gui.GetClassName(hwnd)
        win32gui.EnumPropsEx(hwnd,EnumPropsFunc, self)



def get_active_window():
    _active_window_name = None
    hwnd = win32gui.GetForegroundWindow()
    #name = win32gui.GetWindowText(hwnd)
    return Window(hwnd)

def process_win(hwnd, param):
    #text = win32gui.GetWindowText(hwnd)
    #class_name = win32gui.GetClassName(hwnd)
    #win = Window(hwnd,text)
    parent_window = param[1]
    inspector = param[0]
    win = Window(hwnd)
    parent_window.children.append(win)
    inspector.inspect(win)
    return True

class WinInspector:
    def __init__(self, win):
        self.window = win
        self.children = set()
        self.depth = 0


    def inspect(self, win):
        self.depth += 1
        hwnd = win.hwnd
        if win.hwnd not in self.children:
            url = UrlExtract(hwnd)
                
            print(f"Processing window: hwnd: {hex(hwnd)} ; class: {win.class_name} ; text: {win.text}")
            print("url:", url)
            print(f"Properties: {win.props}")
            self.children.add(hwnd)
            win32gui.EnumChildWindows(hwnd, process_win, (self,win))
        self.depth -= 1


# def printwin(hwnd, param):
#     text = win32gui.GetWindowText(hwnd)
#     class_name = win32gui.GetClassName(hwnd)
#     print(f"{hwnd} : {class_name} : {text}")
#     inspect_window(Window(hwnd,text))
#     return True

#def get_chrome_url():

    # chromeControl = auto.ControlFromHandle(window)
    # edit = chromeControl.
#    return "Chrome"  # https://' + edit.GetValuePattern().Value

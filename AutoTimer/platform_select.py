import sys

if sys.platform in ['Windows', 'win32', 'cygwin']:
    import win_tools as pfm  # pfm stands from platform
elif sys.platform in ['Mac', 'darwin', 'os2', 'os2emx']:
    import mac_tools as pfm  # pfm stands from platform
elif sys.platform in ['linux', 'linux2']:
    import linux_tools as pfm  # pfm stands from platform

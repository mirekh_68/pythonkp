''' 
This file gives the linux support for this repo
'''
import sys
import os
import subprocess
import re

from Window import WindowBase


def get_xprp(param, text):
    match = re.search(param + b"\(\w+\) = (?P<name>.+)", text)
    return match.group("name").decode('utf-8')


# Note: it is possible to use this:
#>> xprop WM_NAME _NET_WM_NAME WM_CLASS

class Window(WindowBase):
    def __init__(self, win_id):
        super().__init__(win_id)

        window = subprocess.Popen(
                ['xprop', '-id', win_id, b"WM_NAME", b"WM_CLASS" ], stdout=subprocess.PIPE)
        stdout, stderr = window.communicate()
        #win = Window(int(win_id.decode('utf-8'), 16))
        #match = re.match(b"WM_NAME\(\w+\) = (?P<name>.+)$", stdout)
        self.text = get_xprp(b"WM_NAME", stdout)
        self.class_name = get_xprp(b"WM_CLASS", stdout)



def get_active_window():
    '''
    returns the details about the window not just the title
    '''
    root = subprocess.Popen(
        ['xprop', '-root', '_NET_ACTIVE_WINDOW'], stdout=subprocess.PIPE)
    stdout, stderr = root.communicate()

    m = re.search(b'^_NET_ACTIVE_WINDOW.* ([\w]+)$', stdout)
    if m == None:
        return None
    wind_id = m.group(1)
    return Window(wind_id)



'''
this file alone can be run without importing other files
uncomment the below lines for linux - works - but activities won't be dumped in json file
(may be it works for other OS also, not sure)
'''
# def run():
#     new_window = None
#     current_window = get_active_window_title()
#     while(True):
#         if new_window != current_window:
#                 print(current_window)
#                 print(type(current_window))
#                 current_window = new_window
#         new_window = get_active_window_title()


# run()
def get_chrome_url_x():
        ''' 
        instead of url the name of the website and the title of the page is returned seperated by '/' 
        '''
        win = get_active_window()
        return None
        # detail_list = detail_full.split(' - ')
        # detail_list.pop()
        # detail_list = detail_list[::-1]
        # _active_window_name = 'Google Chrome -> ' + " / ".join(detail_list)
        # return _active_window_name

# def get_active_window():
#     full_detail = get_active_window()
#     detail_list = None if None else full_detail.split(" - ")
#     new_window_name = detail_list[-1].decode("utf-8")
#     return new_window_name

def inspect_window(win):
    pass
